<?php
// $Id$
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title>
    <?php print $head_title; ?>
  </title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<body class="<?php print $body_classes; ?>">
  <div id="page-wrapper">
    <!-- secondary links -->
    <?php if ($secondary_links): ?>
    <div id="secondary-links">
    <?php print theme('links', $secondary_links); ?>
  </div>
  <?php endif; ?>

  <!-- BEGIN Header -->
  <div id="header-wrapper">

  <!-- logo -->
  <div id="logo">
    <?php if ($logo): ?>
    <a href="<?php print $base_path; ?>" title="<?php print t('Click to return to the Home page'); ?>"><img src="http://ebaker.me.uk/sites/all/themes/fug/logo.gif" alt="<?php print t('Click to return to the Home page '); ?>"/></a>
  <?php endif; ?>
  </div>

  <!-- site name -->
  <div id="sitename">
    <?php if ($site_name): ?>
      <h1><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name; ?></a></h1>
    <?php endif; ?>
  
    <!-- slogan -->
    <?php if ($site_slogan): ?>
      <div id="site-slogan">
        <?php print $site_slogan; ?>
      </div>
      <?php endif; ?>
  </div>

  <!-- logo 2 -->
  <div id="logo2">
    <img src="http://ebaker.me.uk/sites/all/themes/fug/logo2.png" alt="Second site logo" />
  </div>

  <!-- Region: header -->
  <?php if ($header): ?>
  <div id="header-region">
    <?php print $header; ?>
  </div>
  <?php endif; ?>

</div>

<!-- BEGIN Center Content -->
  <div id="main-wrapper">
    <!-- Region: sidebar left -->
    <?php if ($left): ?>
    <div id="sidebar-left-region">
      <?php print $left; ?>
    </div>
    <?php endif; ?>

    <div id="content-wrapper">
      <!-- mission statement -->
      <?php if ($mission): ?>
      <div id="mission">
        <?php print $mission; ?>
      </div>
      <?php endif; ?>
  
      <!-- breadcrumb trail -->
      <?php if ($breadcrumb): ?>
      <div id="breadcrumb">
        <?php print $breadcrumb; ?>
      </div>
      <?php endif; ?>

      <div id="content">
        <!-- tabs -->
        <?php if ($tabs): ?>
        <div class="tabs">
          <?php print $tabs; ?>
        </div>
        <?php endif; ?>

        <!-- title -->
        <?php if ($title): ?>
        <h2 class="content-title"><?php print $title; ?></h2>
        <?php endif; ?>

        <!-- help -->
        <?php print $help; ?>

        <!-- messages -->
        <?php print $messages; ?>

        <!-- Region: content -->
        <?php print $content; ?>
      </div>
    </div>
  </div>
  <!-- END Content Area -->

  <!-- Region: footer -->
  <div id="footer-region">
    <?php print $footer; ?>

    <!-- feed icons -->
    <div id="feed-icons">
      <?php print $feed_icons; ?>
    </div>

    <!-- footer text -->
    <div id="footer-text">
      <?php print $footer_message; ?>
    </div>
  </div>
</div>

<?php print $closure; ?>

</body>
</html>
